﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.WebSockets;
using System.Threading;

namespace LitePlacer
{
    public partial class WebSocket : UserControl
    {
        //public delegate void dataReceived(object,SocketReceivedDataArgs);
        public event EventHandler<SocketReceivedDataArgs> received_data;

        //Uri ws = new Uri("ws://192.168.1.216:81");
        ArraySegment<byte> SendSegment;
        ArraySegment<byte> ReceiveSegment;
        byte[] receiveArray = new byte[200];
        byte[] sendArray = new byte[100];
        Thread dr;
        ClientWebSocket webs;
        Uri ws;

        public bool IsOpen
        {
            get
            {
                if (webs.State == WebSocketState.Open)
                    return true;
                else
                    return false;
            }
        }
        public WebSocket()
        {
            webs = new ClientWebSocket();
            SendSegment = new ArraySegment<byte>(sendArray, 0, 100);
            ReceiveSegment = new ArraySegment<byte>(receiveArray, 0, 100);
            dr = new Thread(() => WaitforDataReceived());
            dr.Name = "DataReceivedThread";
            dr.IsBackground = true;
            dr.Start();
            //Task.Run(() =>
            //{
            //    WaitforDataReceived();
            //});
        }

        public void WebSocketClose()
        {
            dr.Abort();
        }



        public bool Connect(string ip)
        {
            try
            {
                 ws = new Uri("ws://" + ip + ":81");
            }
            catch(Exception ex)
            {
                return false;
            }
            try
            {
                Task output = webs.ConnectAsync(ws, CancellationToken.None);
                output.Wait();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;

        }

        public  bool Close()
        { 
            //Uri ws = new Uri("ws://" + ip + ":81");
            try
            {
                Task output =  webs.CloseAsync(WebSocketCloseStatus.NormalClosure,"closed", CancellationToken.None);
                
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;

        }


        public bool WebSocketSend(string message)
        {
            if (IsOpen)
            {
                if (message[0].Equals('?'))
                {
                    sendArray[0] = (byte)'?';
                    sendArray[1] = 0;
                }
                else
                {
                    PutStringIntoBytes(message);
                }
                webs.SendAsync(SendSegment, WebSocketMessageType.Text, true, CancellationToken.None);
            }
            return true;
        }

        private void PutStringIntoBytes(string text)
        {
            for (int i = 0; i < receiveArray.GetLength(0); i++)
            {
                receiveArray[i] = 0;
            }
            for (int i = 0; i < sendArray.GetLength(0); i++)
            {
                sendArray[i] = 0;
            }
            int count = 0;
            foreach (char c in text)
            {
                sendArray[count++] = (byte)c;
            }
            sendArray[count] = (byte)'\n';
        }

        private  async void WaitforDataReceived()
        {
            bool busy = false;
            for (;;)
            {
                if (this.IsOpen)
                {
                    WebSocketReceiveResult output = await  DataReceived();
                    if (output.MessageType == WebSocketMessageType.Text)
                    {
                        received_data(this, new SocketReceivedDataArgs(Encoding.UTF8.GetString(receiveArray)));
                    }
                }
            }
        }

        private async Task<WebSocketReceiveResult> DataReceived()
        {
            WebSocketReceiveResult test = null;
            test = await webs.ReceiveAsync(ReceiveSegment, CancellationToken.None);
            return test;
        }

    }


    public class SocketReceivedDataArgs : EventArgs
    {
        public SocketReceivedDataArgs(string message)
        {
            Message = message;
        }

        public string Message { get; }

    }


}

